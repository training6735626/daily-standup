package practice;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BasketTest {
    
    @Test
    public void addProductsTest(){
        Product leatherWallet = new Product("Leather Wallet", 1100);
        Product umbrella = new Product("Umbrella", 900);
        Product cigarette = new Product("Cigarette", 200);
        Product honey = new Product("Honey", 100);
        
        Basket basket = new Basket();
        basket.addProducts(leatherWallet);
        basket.addProducts(umbrella);
        basket.addProducts(cigarette);
        basket.addProducts(honey);

        Assert.assertEquals(basket.getProducts().size(), 4);

    }
}
