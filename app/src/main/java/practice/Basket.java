package practice;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    private List<Product> products;

    public Basket() {
       products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProducts(Product product){
        products.add(product);
    }

    public void removeProducts(Product product){
        products.remove(product);
    }

    public double getTotalAmountOfBasket(){
        double totalAmount = 0;
        for (Product product : products) {
            totalAmount += product.getTotalAmount();
        }
        return totalAmount;
    }
    
}
