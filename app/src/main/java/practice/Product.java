package practice;

public class Product {
    private String name;
    private double unitPrice;

    public Product(String name, double unitPrice) {
        this.name = name;
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    private boolean isEligibleForDiscount() {
        return unitPrice >= 500;
    }

    private double getDiscoutedPrice() {
        if (isEligibleForDiscount())
            return 0.05 * unitPrice;
        return 0;
    }
    public double getTotalAmount(){
        return unitPrice - getDiscoutedPrice();
    }
}
